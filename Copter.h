/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#ifndef COPTER_H
#define COPTER_H

#include "SDL2/SDL.h"

#include "util.h"

enum FlightState {
	RISING,
	FALLING
};

class Copter
{
private:
	SDL_Renderer* renderer;
	SDL_Rect draw_rect;
	SDL_Rect src_rect;
	SDL_Texture* texture;
	FlightState state;
	int speed;

private:
	void changeHeight();
	void setTexture();

public:
	Copter(SDL_Renderer*, int, int, int);
	~Copter();

	void draw();
	void handleInput(const SDL_Event&);
	void reset();
	void update();

	SDL_Rect* getRect();
};

#endif
