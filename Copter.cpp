/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "Copter.h"

/* Begin private methods */

void Copter::changeHeight()
{
	const FlightState current_state = this->state;

	switch (this->state) {
		case FALLING:
			this->speed += 2;
			break;
		case RISING:
			this->speed -= 2;
			break;
		default:
			break;
	}

	this->draw_rect.y += speed;
}

void Copter::setTexture()
{
	static bool next_animation = true;

	if (next_animation) {
		if (this->src_rect.x == 192)
			this->src_rect.x = 0;
		else
			this->src_rect.x += 64;
		next_animation = false;
	} else {
		next_animation = true;
		return;
	}
}

/* End private methods */

/* Begin public methods */

Copter::Copter(SDL_Renderer* ren, int size, int x, int y) :
	renderer(ren),
	draw_rect({x, y, size, size}),
	src_rect({0, 0, 64, 64}),
	texture(load_image(ren, "../assets/copter.png")),
	state(FALLING), // Starts with click, so we must rise initially
	speed(0)
{
}

Copter::~Copter()
{
	SDL_DestroyTexture(this->texture);
}

void Copter::draw()
{
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderDrawRect(this->renderer, &(this->draw_rect));
	this->setTexture();
	SDL_RenderCopy(this->renderer, this->texture, &(this->src_rect), &(this->draw_rect));
}

void Copter::handleInput(const SDL_Event& mouse_event)
{
	const Uint8 mouse_button = mouse_event.button.button;

	if (mouse_button == SDL_BUTTON_LEFT) {
		if (mouse_event.type == SDL_MOUSEBUTTONDOWN)
			this->state = RISING;
		else if (mouse_event.type == SDL_MOUSEBUTTONUP)
			this->state = FALLING;
	}

}

void Copter::reset()
{
	this->draw_rect.y = 116;
	this->src_rect.x = 0;
	this->src_rect.y = 0;
	this->state = FALLING;
	this->speed = 0;
}

void Copter::update()
{
	this->changeHeight();
}

SDL_Rect* Copter::getRect() { return &(this->draw_rect); }

/* End public methods */
