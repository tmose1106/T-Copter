/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <cstdio>

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"

#include "Cavern.h"
#include "Copter.h"
#include "HUD.h"

/* Start constants */

const int COPTER_SIZE = 128;
const int FPS = 60;
const int FRAME_DURATION = 1000 / FPS;
const int WINDOW_HEIGHT = 360;
const int WINDOW_WIDTH = 640;

const Uint32 MS_PER_UPDATE = 80;

/* End constants */

int main(int argc, char *argv[])
{
  SDL_Init(SDL_INIT_VIDEO); // Initialize video output
  IMG_Init(IMG_INIT_PNG); // Initialize image loading
  TTF_Init(); // Initialize font rendering

  SDL_Window *window = SDL_CreateWindow(
    "T-Copter 0.0.4",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    WINDOW_WIDTH,
    WINDOW_HEIGHT,
    SDL_WINDOW_SHOWN
  );

  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

  // Get our cavern generator
  Cavern cave = { renderer, WINDOW_WIDTH, WINDOW_HEIGHT };

  // Get our T-Copter object
  Copter copter = { renderer, COPTER_SIZE, 100, ((WINDOW_HEIGHT / 2) - (COPTER_SIZE / 2)) };

  int score = 0;
  HUD hud = { renderer, "../assets/FreeMono.ttf", WINDOW_WIDTH, WINDOW_HEIGHT, &score };

  bool start_game = false; // When the player clicks
  bool end_game = false; // When the player loses
  bool quit_game = false; // When to close the game window
  bool paused = false;

  Uint32 frame_time = SDL_GetTicks();

  while (!quit_game) {

    Uint32 previous = SDL_GetTicks();
    Uint32 lag = 0;

    while (!end_game && !quit_game) {

      Uint32 current = SDL_GetTicks();
      lag += current - previous;
      previous = current;

      /* Begin input handling */
      SDL_Event event;
      SDL_PollEvent(&event);

      switch (event.type) {
        case SDL_QUIT:
          quit_game = true;
          break;
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
          copter.handleInput(event);
          break;
        case SDL_KEYDOWN:
          if ((event.key.keysym.sym == SDLK_ESCAPE) && (event.key.repeat == 0))
            lag = 0;
            paused = true;
          break;
        case SDL_KEYUP:
          if ((event.key.keysym.sym == SDLK_ESCAPE) && (event.key.repeat == 0) && paused)
            lag = 0;
            paused = false;
        default:
          break;
      }
      /* End input handling */

      /* Start updating objects */
      while ((lag >= MS_PER_UPDATE) && !(paused)) {
        cave.update();
        copter.update();
        hud.update();

        if (cave.detectCollision(copter.getRect()))
          end_game = true;

        score += 1;

        lag -= MS_PER_UPDATE;
      }
      /* End updating objects */

      /* Start render */
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
      SDL_RenderClear(renderer);

      cave.draw();
      hud.draw();
      copter.draw();

      SDL_RenderPresent(renderer);
      /* End render */
    }

    if (!quit_game) {
      start_game = end_game = false;
      copter.reset();
      score = 0;
    }

  }

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  atexit(IMG_Quit);
  atexit(TTF_Quit); // Quit TTF at very end so fonts can be closed
  SDL_Quit();

  return 0;
}
