********
T-Copter
********

A Free (as in freedom) Cross-Platform Helicopter Game

Dependencies
============

Runtime
-------

* SDL2
* SDL2_image
* SDL2_ttf

Build
-----

* cmake
* GNU Make

Building From Source
====================

T-Copter can be built using the following commands:

::

  mkdir build
  cd build
  cmake ..
  make

Licensing
=========

The source code for this project is released under the terms of the
Mozilla Public License (MPL), v. 2.0.
For more information, take a look at the `LICENSE.txt`_ file.

.. _`LICENSE.txt`: ./LICENSE.txt
