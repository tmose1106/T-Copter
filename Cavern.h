/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#ifndef CAVE_H
#define CAVE_H

#include <vector>

#include "SDL2/SDL.h"

const int COLUMN_COUNT = 20;

struct CavernColumn {
	int top_offset;
	int bottom_offset;
	int center_top_offset;
	int center_bottom_offset;
};

class Cavern
{
private:
	SDL_Renderer* renderer;
	int window_height, window_width;
	int column_width, column_offset;
	std::vector<CavernColumn*> columns;

public:
	Cavern(SDL_Renderer*, int, int);
	~Cavern();

	void draw();
	void update();

	bool detectCollision(SDL_Rect*);
};

#endif
