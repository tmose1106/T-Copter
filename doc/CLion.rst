*************
Getting CLion
*************

This tutorial assumes that you have already followed the MSYS2 tutorial.

Downloading CLion
=================

* Visit the `JetBrains website`_
* Sign up for an account
* Optionally register for a student account
* Visit the `CLion download page`_

.. _`CLion download page`: https://www.jetbrains.com/clion/download/
.. _`JetBrains website`: https://www.jetbrains.com/

Wizard Configuration
====================

The only thing to changed in the install wizard is the toolchain. Call it
``MSYS2``. Select the MinGW environment in the provided drop-down menu.
Assuming everything was installed correctly, CLion will be able to find all
of the necessary tools. But if not, it should look something like below.

============  ====================================================
Field Name    Default Value
============  ====================================================
MinGW         ``C:\msys64\mingw64``
CMake         ``C:\msys64\mingw64\bin\cmake.exe``
Make          Detected: ``C:\msys64\mingw64\bin\mingw32-make.exe``
C Compiler    Detected: ``C:\msys64\mingw64\bin\gcc.exe``
C++ Compiler  Detected: ``C:\msys64\mingw64\bin\g++.exe``
Debugger      MinGW-w64 GDB ``(C:\msys64\mingw64\bin\gdb.exe)``
============  ====================================================
