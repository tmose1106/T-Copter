*************
Getting MSYS2
*************

These steps are necessary in order to build T-Copter on Windows.

Installing MSYS2
================

* Go to `http://www.msys2.org/`_
* Download the x86_64 installer
* Run the installation wizard

Now that it is installed, run the *"MSYS2 MSYS"* executable from the start
menu. This will open a Bash shell where you can run:

::

  pacman -Syyu --noconfirm

This will update the system to the latest version of everything. After this
completes, you may be prompted to restart MSYS. Simply exit the window.

.. _`http://www.msys2.org/`: http://www.msys2.org/

Installing Build Tools
======================

We must first install our compiler and other tools. MSYS2 makes this kinda
easy by including a *toolchain* group. Open the *"MSYS2 MinGW 64-Bit"*
executable from the start menu and run the command:

::

  pacman -S mingw-w64-x86_64-{toolchain,cmake,make}

Installing SDL Dependencies
===========================

Now to install the SDL dependencies, in the same terminal as previously, we
can use the command:

::

  pacman -S mingw-w64-x86_64-SDL2{,_{gfx,image,ttf}}

Configuring and Building the Project
====================================

Now we can change to the directory where the project is installed. In this
case, the project is installed under ``C:\Users\tmose\Projects\T-Copter``.
In MSYS, this translates to ``/C/Users/tmose/Projects/T-Copter``.

::

  cd /C/Users/tmose/Projects/T-Copter

Now, inside of this directory, we can run the following commands to
configure and build the project:

::

  mkdir build
  cd build
  cmake -G "Unix Makefiles" -DCMAKE_MAKE_PROGRAM=mingw32-make ..
  make

A successful build should look something like:

::

  tmose@DESKTOP-3AFEILG MINGW64 /C/Users/tmose/Projects/T-Copter
  $ mkdir build

  tmose@DESKTOP-3AFEILG MINGW64 /C/Users/tmose/Projects/T-Copter
  $ cd build/

  tmose@DESKTOP-3AFEILG MINGW64 /C/Users/tmose/Projects/T-Copter/build
  $ cmake -G "Unix Makefiles" -DCMAKE_MAKE_PROGRAM=mingw32-make ..
  -- The C compiler identification is GNU 7.3.0
  -- The CXX compiler identification is GNU 7.3.0
  -- Check for working C compiler: C:/msys64/mingw64/bin/cc.exe
  -- Check for working C compiler: C:/msys64/mingw64/bin/cc.exe -- works
  -- Detecting C compiler ABI info
  -- Detecting C compiler ABI info - done
  -- Detecting C compile features
  -- Detecting C compile features - done
  -- Check for working CXX compiler: C:/msys64/mingw64/bin/c++.exe
  -- Check for working CXX compiler: C:/msys64/mingw64/bin/c++.exe -- works
  -- Detecting CXX compiler ABI info
  -- Detecting CXX compiler ABI info - done
  -- Detecting CXX compile features
  -- Detecting CXX compile features - done
  -- Found PkgConfig: C:/msys64/mingw64/bin/pkg-config.exe (found version "0.29.2")
  -- Checking for modules 'sdl2;SDL2_ttf;SDL2_gfx;SDL2_image'
  --   Found sdl2, version 2.0.8
  --   Found SDL2_gfx, version 1.0.1
  --   Found SDL2_image, version 2.0.3
  --   Found SDL2_ttf, version 2.0.14
  -- Configuring done
  -- Generating done
  -- Build files have been written to: C:/Users/tmose/Projects/T-Copter/build

  tmose@DESKTOP-3AFEILG MINGW64 /C/Users/tmose/Projects/T-Copter/build
  $ make
  Scanning dependencies of target T_Copter
  [ 14%] Building CXX object CMakeFiles/T_Copter.dir/Background.cpp.obj
  [ 28%] Building CXX object CMakeFiles/T_Copter.dir/Cavern.cpp.obj
  [ 42%] Building CXX object CMakeFiles/T_Copter.dir/Copter.cpp.obj
  [ 57%] Building CXX object CMakeFiles/T_Copter.dir/HUD.cpp.obj
  [ 71%] Building CXX object CMakeFiles/T_Copter.dir/main.cpp.obj
  [ 85%] Building CXX object CMakeFiles/T_Copter.dir/util.cpp.obj
  [100%] Linking CXX executable T_Copter.exe
  [100%] Built target T_Copter

Running the Executable
======================

The executable can now be run one of two ways: on the command line or
through File Explorer.

To run on the command line, simply enter:

::

  ./T_Copter.exe

Running through the File Explorer is as simple as double clicking on the
``T_Copter.exe`` file.
