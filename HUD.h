/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#ifndef HUD_H
#define HUD_H

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

class HUD
{
private:
	SDL_Renderer* renderer;
	TTF_Font* font;
	int window_width, window_height;
	int* score_value;

	SDL_Texture* score_label_text;
	SDL_Texture* score_value_text;
	// SDL_Texture* high_score_label_text;
	// SDL_Texture* high_score_value_text;

public:
	HUD(SDL_Renderer*, const char*, int, int, int*);
	~HUD();

	void draw();
	void update();
};

#endif
