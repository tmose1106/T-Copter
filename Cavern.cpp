/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "Cavern.h"

#include <iostream>

/* Begin constant definitions */

// RGB values for the cavern blocks
const Uint8 BLOCK_R = 100;
const Uint8 BLOCK_G = 250;
const Uint8 BLOCK_B = 65;

// RGB values for the cavern block stripes
const Uint8 BLOCK_STRIPE_R = 44;
const Uint8 BLOCK_STRIPE_G = 122;
const Uint8 BLOCK_STRIPE_B = 200;

/* End constant definitions */

/* Begin private methods */
/* End private methods */

/* Begin public methods */

Cavern::Cavern(SDL_Renderer* ren, int win_w, int win_h)
	: renderer(ren), window_height(win_h), window_width(win_w),
		column_width(win_w / COLUMN_COUNT), column_offset(0)
{
	for (int column_index = 0; column_index < COLUMN_COUNT + 1; column_index++) {
		CavernColumn* column = new CavernColumn;
		column->top_offset = 30 + column_index;
		column->bottom_offset = 30 - column_index;
		this->columns.emplace_back(column);

		if (column_index != 10)
			column->center_top_offset = column->center_bottom_offset = 0;
		else
			column->center_top_offset = column->center_bottom_offset = 100;
	}
}

Cavern::~Cavern()
{
}

void Cavern::draw()
{
	SDL_Rect* block = new SDL_Rect;

	block->w = this->column_width;

	SDL_SetRenderDrawColor(this->renderer, BLOCK_R, BLOCK_G, BLOCK_B, 255);

	int columns_to_draw = COLUMN_COUNT;

	if (column_offset)
		columns_to_draw = COLUMN_COUNT + 1;
	else
		columns_to_draw = COLUMN_COUNT;

	for (int column_index = 0; column_index < columns_to_draw; column_index++) {
		CavernColumn* column = this->columns.at(column_index);

		block->x = column_index * this->column_width - (this->column_offset * 5);

		// Calculate position and draw top block
		block->y = 0;
		block->h = column->top_offset;

		SDL_RenderFillRect(this->renderer, block);
		// SDL_RenderFillRect(this->renderer, block);

		// Calculate position and draw bottom block
		int bottom_column_height = this->window_height - column->bottom_offset;

		block->y = bottom_column_height;
		block->h = this->window_height - bottom_column_height;

		SDL_RenderFillRect(this->renderer, block);

		// Draw an optional center block
		if ((column->center_top_offset == 0) && (column->center_bottom_offset == 0))
			continue;
		else {
			block->y = column->center_top_offset;
			block->h = (this->window_height - column->center_bottom_offset) - column->center_top_offset;

			SDL_RenderFillRect(this->renderer, block);
		}
	}
}

void Cavern::update()
{
	if (this->column_offset < 6)
		this->column_offset += 1;
	else
		this->column_offset = 0;
}

bool Cavern::detectCollision(SDL_Rect* collider)
/* Detect collisons against bottom and top rectangles */
{
	const int collider_top = collider->y;
	const int collider_bottom = collider_top + collider->h;

	const int ceiling_min = 48;
	const int floor_max = 312;

	if (collider_bottom > floor_max)
		return true;

	if (collider_top < ceiling_min)
		return true;

	return false;
}

/* End public methods */
