/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <iomanip>
#include <sstream>

#include "HUD.h"

#include "util.h"

HUD::HUD(SDL_Renderer* ren, const char* font_path, int win_w, int win_h, int* score)
	: renderer(ren), font(TTF_OpenFont(font_path, 56)),
		window_width(win_w), window_height(win_h),
		score_value(score)
{
	TTF_SetFontHinting(this->font, TTF_HINTING_MONO);

	this->score_label_text = load_text(this->renderer, this->font, "Score:", 0, 0, 0);
	this->score_value_text = load_text(this->renderer, this->font, "0000", 0, 0, 0);
	// this->high_score_label_text = load_text(this->renderer, this->font, "High Score: ", 0, 0, 0);
}

HUD::~HUD()
{
	SDL_DestroyTexture(this->score_label_text);
	// SDL_DestroyTexture(this->high_score_label_text);
	TTF_CloseFont(this->font);
}

void HUD::draw()
{
	SDL_Rect score_label_draw_rect = {2, (window_height - 42), 100, 40};
	SDL_Rect score_value_draw_rect = {102, (window_height - 42), 80, 40};

	SDL_SetRenderDrawColor(this->renderer, 255, 255, 255, 128);
	SDL_SetRenderDrawBlendMode(this->renderer, SDL_BLENDMODE_BLEND);
	// Draw the text background rectangle
	SDL_RenderFillRect(this->renderer, &score_label_draw_rect);
	SDL_RenderFillRect(this->renderer, &score_value_draw_rect);
	SDL_SetRenderDrawBlendMode(this->renderer, SDL_BLENDMODE_NONE);
	// Draw the score text
	SDL_RenderCopy(this->renderer, this->score_label_text, NULL, &score_label_draw_rect);
	SDL_RenderCopy(this->renderer, this->score_value_text, NULL, &score_value_draw_rect);
}

void HUD::update()
{
	std::ostringstream ss;
	ss << std::setw(4) << std::setfill('0') << *(this->score_value);
	this->score_value_text = load_text(this->renderer, this->font, ss.str().c_str(), 0, 0, 0);
}
