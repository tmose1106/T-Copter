***********
Attribution
***********

* The `main theme`_ was found on freesound.org as the "Random sound of the
  day".
* The `explosion effect`_ was found on freesound.org via a search.


.. _`explosion effect`: https://freesound.org/s/399303/
.. _`main theme`: https://freesound.org/s/194306/
