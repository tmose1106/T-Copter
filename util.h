/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

SDL_Texture* load_image(SDL_Renderer*, const char*);
SDL_Texture* load_text(SDL_Renderer*, TTF_Font*, const char*, Uint8, Uint8, Uint8);
