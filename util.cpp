/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "util.h"

#include "SDL2/SDL_image.h"

SDL_Color RGBAtoColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
  SDL_Color color;

  color.r = r;
  color.g = g;
  color.b = b;
  color.a = a;

  return color;
}

SDL_Texture* load_image(SDL_Renderer* renderer, const char* asset_path)
{
  SDL_Surface *tmp_surface = IMG_Load(asset_path);
  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, tmp_surface);
  SDL_FreeSurface(tmp_surface);

  return texture;
}

SDL_Texture* load_text(SDL_Renderer* renderer, TTF_Font* font, const char* text,
  Uint8 r, Uint8 g, Uint8 b)
{
  SDL_Color font_color = RGBAtoColor(r, g, b, 255);
  SDL_Surface *tmp_surface = TTF_RenderText_Blended(font, text, font_color);
  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, tmp_surface);
  SDL_FreeSurface(tmp_surface);

  return texture;
}
